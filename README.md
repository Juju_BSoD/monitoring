# monitoring

# Requirements

Container registry login

```
docker login registry.gitlab.com/cracs_airbus/ngms -u cracs -p sb1HzR-X7jc_DaYsMDzQ
```

Container network host

```
docker network create ngms-network
```




# Run dev local database

In a new terminal, go to the root of project and run following command to launch database docker

```
sudo docker run --rm --pull always -p 6543:5432 \
    --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 \
	-e POSTGRES_DB=ngms \
	-e POSTGRES_USER=root \
	-e POSTGRES_PASSWORD=root \
	--name postgis-ngms --network=ngms-network registry.gitlab.com/cracs_airbus/ngms/dockerfiles/postgis
```

# Run test local database

In a new terminal, go to the root of project and run following command to launch database docker

```
docker run --rm --pull always -p 6544:5432 \
    --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 \
	-e POSTGRES_DB=test-ngms \
	-e POSTGRES_USER=test-ngms \
	-e POSTGRES_PASSWORD=test-ngms \
	--name postgis-ngms-test registry.gitlab.com/cracs_airbus/ngms/dockerfiles/postgis
```

# Run local nominatim

In a new terminal, go to the root of project and run following command to launch nominatim docker

```
docker run --rm --pull always -p 18080:8080 \
    --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 \
	--network=ngms-network \
	--name nomintim-ngms registry.gitlab.com/cracs_airbus/ngms/dockerfiles/nominatim
```

# Run dockerized carto-poc

In a new terminal, apres run an docker of database and nominatim

```
sudo docker run --rm --pull always -p 15080:15080 \
    --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 \
	--network=ngms-network \
	--name carto-poc-ngms registry.gitlab.com/cracs_airbus/ngms/carto-poc
```

# Run monitoring of docker
```
docker run   --volume=/:/rootfs:ro   --volume=/var/run:/var/run:ro   --volume=/sys:/sys:ro   --volume=/var/lib/docker/:/var/lib/docker:ro   --volume=/dev/disk/:/dev/disk:ro   --publish=8080:8080   --detach=true   --name=cadvisor   --privileged   --device=/dev/kmsg  --log-driver=gelf --log-opt gelf-address=udp://127.0.0.1:12201 gcr.io/cadvisor/cadvisor:v0.36.0
```

# Run log centralisation
```

docker-compose up 


```